import torch
from .spectral_normalization import SpectralNorm

class Discriminator(torch.nn.Module):
    def __init__(self, input_channels=3):
        super(Discriminator, self).__init__()
        
        #Hint: Hint: Apply spectral normalization to convolutional layers. Input to SpectralNorm should be your conv nn module
        ####################################
        #          YOUR CODE HERE          #
        ####################################
        self.conv1 = torch.nn.Conv2d(input_channels, 128, kernel_size = 4, stride = 2, padding = 1)
        self.conv2 = torch.nn.Conv2d(128, 256, kernel_size = 4, stride = 2, padding = 1)
        self.conv3 = torch.nn.Conv2d(256, 512, kernel_size = 4, stride = 2, padding = 1)
        self.conv4 = torch.nn.Conv2d(512, 1024, kernel_size = 4, stride = 2, padding = 1)
        self.conv5 = torch.nn.Conv2d(1024, 1, kernel_size = 4, stride = 1, padding = 0)
        self.batchnorm1 = torch.nn.BatchNorm2d(256)
        self.batchnorm2 = torch.nn.BatchNorm2d(512)
        self.batchnorm3 = torch.nn.BatchNorm2d(1024)
        self.relu1 = torch.nn.LeakyReLU()
        self.relu2 = torch.nn.LeakyReLU()
        self.relu3 = torch.nn.LeakyReLU()
        self.relu4 = torch.nn.LeakyReLU()
        ##########       END      ##########
    
    def forward(self, x):
        
        ####################################
        #          YOUR CODE HERE          #
        ####################################
        x = self.relu1(self.conv1(x))
        x = self.relu2(self.conv2(x))
        x = self.batchnorm1(x)
        x = self.relu3(self.conv3(x))
        x = self.batchnorm2(x)
        x = self.relu4(self.conv4(x))
        x = self.batchnorm3(x)
        x = (self.conv5(x)).float()
        ##########       END      ##########
        
        return x


class Generator(torch.nn.Module):
    def __init__(self, noise_dim, output_channels=3):
        super(Generator, self).__init__()    
        self.noise_dim = noise_dim
        
        ####################################
        #          YOUR CODE HERE          #
        ####################################
        self.conv1 = torch.nn.ConvTranspose2d(self.noise_dim, 1024, kernel_size = 4, stride = 1, padding = 0)
        self.conv2 = torch.nn.ConvTranspose2d(1024, 512, kernel_size = 4, stride = 2, padding = 1)
        self.conv3 = torch.nn.ConvTranspose2d(512, 256, kernel_size = 4, stride = 2, padding = 1)
        self.conv4 = torch.nn.ConvTranspose2d(256, 128, kernel_size = 4, stride = 2, padding = 1)
        self.conv5 = torch.nn.ConvTranspose2d(128, 3, kernel_size = 4, stride = 2, padding = 1)
        self.batchnorm1 = torch.nn.BatchNorm2d(1024)
        self.batchnorm2 = torch.nn.BatchNorm2d(512)
        self.batchnorm3 = torch.nn.BatchNorm2d(256)
        self.batchnorm4 = torch.nn.BatchNorm2d(128)
        self.relu1 = torch.nn.ReLU()
        self.relu2 = torch.nn.ReLU()
        self.relu3 = torch.nn.ReLU()
        self.relu4 = torch.nn.ReLU()
        self.tanh = torch.nn.Tanh()
        ##########       END      ##########
    
    def forward(self, x):
        
        ####################################
        #          YOUR CODE HERE          #
        ####################################
        x = self.relu1(self.conv1(x))
        x = self.batchnorm1(x)
        x = self.relu2(self.conv2(x))
        x = self.batchnorm2(x)
        x = self.relu3(self.conv3(x))
        x = self.batchnorm3(x)
        x = self.relu4(self.conv4(x))
        x = self.batchnorm4(x)
        x = self.tanh(self.conv5(x))
        ##########       END      ##########
        
        return x
    

