import torch
from torch.nn.functional import binary_cross_entropy_with_logits as bce_loss

def discriminator_loss(logits_real, logits_fake):
    """
    Computes the discriminator loss.
    
    You should use the stable torch.nn.functional.binary_cross_entropy_with_logits 
    loss rather than using a separate softmax function followed by the binary cross
    entropy loss.
    
    Inputs:
    - logits_real: PyTorch Tensor of shape (N,) giving scores for the real data.
    - logits_fake: PyTorch Tensor of shape (N,) giving scores for the fake data.
    
    Returns:
    - loss: PyTorch Tensor containing (scalar) the loss for the discriminator.
    """
    
    loss = None
    
    ####################################
    #          YOUR CODE HERE          #
    ####################################
    # We use sigmoid + BCE
    # Compute the loss for true logits.
    size = logits_real.size()
    target = torch.ones(size).to("cuda:0")
    val1 = bce_loss(logits_real, target)
    # Compute the loss for and fake logits.
    size = logits_real.size()
    target = torch.zeros(size).to("cuda:0")
    val2 = bce_loss(logits_fake, target)
    # Loss is the sum of two individual losses.
    loss = val1 + val2
    ##########       END      ##########
    
    return loss

def generator_loss(logits_fake):
    """
    Computes the generator loss.
    
    You should use the stable torch.nn.functional.binary_cross_entropy_with_logits 
    loss rather than using a separate softmax function followed by the binary cross
    entropy loss.

    Inputs:
    - logits_fake: PyTorch Tensor of shape (N,) giving scores for the fake data.
    
    Returns:
    - loss: PyTorch Tensor containing the (scalar) loss for the generator.
    """
    
    loss = None
    
    ####################################
    #          YOUR CODE HERE          #
    ####################################
    size = logits_fake.size()
    target = torch.ones(size).to("cuda:0")
    loss = bce_loss(logits_fake, target)
    ##########       END      ##########
    
    return loss


def ls_discriminator_loss(scores_real, scores_fake):
    """
    Compute the Least-Squares GAN loss for the discriminator.
    
    Inputs:
    - scores_real: PyTorch Tensor of shape (N,) giving scores for the real data.
    - scores_fake: PyTorch Tensor of shape (N,) giving scores for the fake data.
    
    Outputs:
    - loss: A PyTorch Tensor containing the loss.
    """
    
    loss = None
    
    ####################################
    #          YOUR CODE HERE          #
    ####################################
    size1 = scores_real.size()[0]
    size2 = scores_fake.size()[0]
    # Compute the loss for real scores.
    target = torch.ones(size1).to("cuda:0")
    val1 = 1/2 * (((scores_real - target) ** 2).sum().float())
    val1 = val1 / size1
    # Compute the loss for fake scores.
    val2 = 1/2 * ((scores_fake ** 2).sum().float())
    val2 = val2 / size2
    # Sum to get the total loss.
    loss = (val1 + val2)
    ##########       END      ##########
    
    return loss

def ls_generator_loss(scores_fake):
    """
    Computes the Least-Squares GAN loss for the generator.
    
    Inputs:
    - scores_fake: PyTorch Tensor of shape (N,) giving scores for the fake data.
    
    Outputs:
    - loss: A PyTorch Tensor containing the loss.
    """
    
    loss = None
    
    ####################################
    #          YOUR CODE HERE          #
    ####################################
    size = scores_fake.size()[0]
    target = torch.ones(size).to("cuda:0")
    loss = 1/2 * (((scores_fake - target) ** 2).sum().float())
    loss = loss / size
    ##########       END      ##########
    
    return loss
