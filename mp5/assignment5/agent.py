"""
with reference to pytorch tutorial of DQN
https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html
"""
import random
import torch
import numpy as np
from collections import deque
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from memory import ReplayMemory, ReplayMemoryLSTM
from model import DQN, DQN_LSTM
from utils import find_max_lives, check_live, get_frame, get_init_state
from config import *
import os

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Agent():
    def __init__(self, action_size):
        self.action_size = action_size

        # These are hyper parameters for the DQN
        self.discount_factor = 0.99
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.explore_step = 500000
        self.epsilon_decay = (self.epsilon - self.epsilon_min) / self.explore_step
        self.train_start = 100000
        self.update_target = 1000

        # Generate the memory
        self.memory = ReplayMemory()

        # Create the policy net
        self.policy_net = DQN(action_size)
        self.policy_net.to(device)

        self.optimizer = optim.Adam(params=self.policy_net.parameters(), lr=learning_rate)
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=scheduler_step_size, gamma=scheduler_gamma)

    def load_policy_net(self, path):
        self.policy_net = torch.load(path)

    """Get action using policy net using epsilon-greedy policy"""
    def get_action(self, state):
        if np.random.rand() <= self.epsilon:
            ### CODE #### 
            # Choose a random action
            a=random.randrange(self.action_size)
        else:
            ### CODE ####
            # Choose the best action
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            state=torch.from_numpy(state).unsqueeze(0).cuda()
            a=self.policy_net(state).max(1)[1].view(1,-1)
            a=a.detach().cpu()
        return int(a)

    # pick samples randomly from replay memory (with batch_size)
    def train_policy_net(self, frame):
        if self.epsilon > self.epsilon_min:
            self.epsilon -= self.epsilon_decay

        mini_batch = self.memory.sample_mini_batch(frame)
        mini_batch = np.array(mini_batch,dtype=object).transpose()
        history = np.stack(mini_batch[0], axis=0)
        states = np.float32(history[:, :4, :, :]) / 255.
        states = torch.from_numpy(states).cuda()
        actions = list(mini_batch[1])
        actions = torch.LongTensor(actions).cuda()
        rewards = list(mini_batch[2])
        rewards = torch.FloatTensor(rewards).cuda()
        next_states = np.float32(history[:, 1:, :, :]) / 255.
        dones = mini_batch[3] # checks if the game is over
        mask = torch.tensor(list(map(int, dones==False)),dtype=torch.bool)
        non_final_next_states=torch.tensor(np.array([next_states[i] for i in range (len(mask)) if mask[i]==1])).cuda()
        # Compute Q(s_t, a), the Q-value of the current state
        ### CODE ####
        #gether: shape of policy network output is Sxa, choose a according to actions list
        state_action_values=self.policy_net(states).gather(1,actions.unsqueeze(-1).expand(len(states),3)).mean(-1)
        # Compute Q function of next state
        ### CODE ####
        max_next_states_values=torch.zeros(len(states),device=device)
        # Find maximum Q-value of action at next state from policy net
        ### CODE ####
        # Expected values of actions for non_final_next_states are computed based
        # on the "older" target_net; selecting their best reward with max(1)[0].
        # This is merged based on the mask, such that we'll have either the expected
        # state value or 0 in case the state was final.
        max_next_states_values[mask]=self.policy_net(non_final_next_states).max(1)[0].detach()
        expected_state_action_values = rewards+(self.discount_factor * max_next_states_values)
        # Compute the Huber Loss
        ### CODE ####
        criterion = nn.SmoothL1Loss()
        loss = criterion(state_action_values, expected_state_action_values)
        # Optimize the model, .step() both the optimizer and the scheduler!
        ### CODE ####
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1) # gradients clipping, constraint the grad into [-1,1]
        self.optimizer.step()
        self.scheduler.step()


class LSTM_Agent(Agent):
    def __init__(self, action_size):
        super().__init__(action_size)
        # Generate the memory
        self.memory = ReplayMemoryLSTM()

        # Create the policy net
        self.policy_net = DQN_LSTM(action_size)
        self.policy_net.to(device)

        self.optimizer = optim.Adam(params=self.policy_net.parameters(), lr=learning_rate)
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=scheduler_step_size, gamma=scheduler_gamma)


    """Get action using policy net using epsilon-greedy policy"""
    def get_action(self, state, hidden=None):
        ### CODE ###
        if np.random.rand() <= self.epsilon:
            ### CODE #### 
            # Choose a random action
            a=random.randrange(self.action_size)
        else:
            ### CODE ####
            # Choose the best action
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            state=torch.from_numpy(state).unsqueeze(0).cuda()
            a, hidden=self.policy_net(state)
            a = a.max(1)[1].view(1,-1)
            a=a.detach().cpu()
        return a, hidden
        # Nearly same as that for Agent

    # pick samples randomly from replay memory (with batch_size)
    def train_policy_net(self, frame):
        if self.epsilon > self.epsilon_min:
            self.epsilon -= self.epsilon_decay

        mini_batch = self.memory.sample_mini_batch(frame)
        mini_batch = np.array(mini_batch).transpose()

        history = np.stack(mini_batch[0], axis=0)
        states = np.float32(history[:, :lstm_seq_length, :, :]) / 255.
        states = torch.from_numpy(states).cuda()
        actions = list(mini_batch[1])
        actions = torch.LongTensor(actions).cuda()
        rewards = list(mini_batch[2])
        rewards = torch.FloatTensor(rewards).cuda()
        next_states = np.float32(history[:, 1:, :, :]) / 255.
        dones = mini_batch[3] # checks if the game is over
        mask = torch.tensor(list(map(int, dones==False)),dtype=torch.bool)
        non_final_next_states=torch.tensor(np.array([next_states[i] for i in range (len(mask)) if mask[i]==1])).cuda()
        ### All the following code is nearly same as that for Agent

        #gether: shape of policy network output is Sxa, choose a according to actions list
        state_action_values=self.policy_net(states)[0].gather(1,actions.unsqueeze(-1).expand(len(states),3)).mean(-1)
        # Compute Q function of next state
        ### CODE ####
        max_next_states_values=torch.zeros(len(states),device=device)
        # Find maximum Q-value of action at next state from policy net
        ### CODE ####
        # Expected values of actions for non_final_next_states are computed based
        # on the "older" target_net; selecting their best reward with max(1)[0].
        # This is merged based on the mask, such that we'll have either the expected
        # state value or 0 in case the state was final.
        max_next_states_values[mask]=self.policy_net(non_final_next_states)[0].max(1)[0].detach()
        expected_state_action_values = rewards+(self.discount_factor * max_next_states_values)
        # Compute the Huber Loss
        ### CODE ####
        criterion = nn.SmoothL1Loss()
        loss = criterion(state_action_values, expected_state_action_values)
        # Optimize the model, .step() both the optimizer and the scheduler!
        ### CODE ####
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1) # gradients clipping, constraint the grad into [-1,1]
        self.optimizer.step()
        self.scheduler.step()


