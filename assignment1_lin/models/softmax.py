"""Softmax model."""

import numpy as np


class Softmax:
    def __init__(self, n_class: int, lr: float, epochs: int, reg_const: float):
        """Initialize a new classifier.

        Parameters:
            n_class: the number of classes
            lr: the learning rate
            epochs: the number of epochs to train for
            reg_const: the regularization constant
        """
        self.w = None  # TODO: change this
        self.N = 1
        self.lr = lr
        self.epochs = epochs
        self.reg_const = reg_const
        self.n_class = n_class

    def softmax(vector):
        """Calculate the softmax of the vector.

        Returns:
            An ndarray of the same size as vector.
        """
        Sum = 0
        result = np.zeros(len(vector))
        max_val = max(vector)
        vector -= max_val
        for i in range(len(vector)):
            Sum += np.e ** vector[i]
        for i in range(len(vector)):
            result[i] = np.e ** vector[i] / Sum
        return result

    def calc_gradient(self, X_train: np.ndarray, y_train: np.ndarray) -> np.ndarray:
        """Calculate gradient of the softmax loss.

        Inputs have dimension D, there are C classes, and we operate on
        mini-batches of N examples.

        Parameters:
            X_train: a numpy array of shape (N, D) containing a mini-batch
                of data
            y_train: a numpy array of shape (N,) containing training labels;
                y[i] = c means that X[i] has label c, where 0 <= c < C

        Returns:
            gradient with respect to weights w; an array of same shape as w
        """
        # TODO: implement me
        Num = len(y_train)                                                     # The size of the current mini-batch.
        gradient = np.zeros((self.w.shape[0], self.w.shape[1]))
        for i in range(Num):
            x = X_train[i]                                                     # Current data.
            y = y_train[i]                                                     # Current label.
            raw_result = np.dot(self.w, x)
            prob = Softmax.softmax(raw_result)
            for j in range(self.n_class):                                      # Fill in the gradient.
                gradient[j] += prob[j] * x
            gradient[y] -= x
            gradient += self.reg_const * self.w                                # Gradient respect to regularization.
        gradient *= 1/Num
        return gradient

    def train(self, X_train: np.ndarray, y_train: np.ndarray):
        """Train the classifier.

        Hint: operate on mini-batches of data for SGD.

        Parameters:
            X_train: a numpy array of shape (N, D) containing training data;
                N examples with D dimensions
            y_train: a numpy array of shape (N,) containing training labels
        """
        # TODO: implement me
        self.w = np.zeros((self.n_class, X_train.shape[1]+1))                    # The matrix is of n_class * len(onedata).
        one_vec = np.ones(X_train.shape[0])
        X_train = np.c_[X_train, one_vec]
        remain_size = len(y_train) % self.N
        num_batch = int(len(y_train) / self.N)
        for i in range(self.epochs):
            for j in range(num_batch):
                gradient = Softmax.calc_gradient(self, X_train[j*self.N : (j+1)*self.N], y_train[j*self.N : (j+1)*self.N])  # Generating a mini-batch.
                self.w -= self.lr * gradient
            if (remain_size > 0):
                gradient = Softmax.calc_gradient(self, X_train[num_batch*self.N : len(y_train)], y_train[num_batch*self.N : len(y_train)])      #Consider about the last batch.
                self.w -= self.lr * gradient
        return

    def predict(self, X_test: np.ndarray) -> np.ndarray:
        """Use the trained weights to predict labels for test data points.

        Parameters:
            X_test: a numpy array of shape (N, D) containing testing data;
                N examples with D dimensions

        Returns:
            predicted labels for the data in X_test; a 1-dimensional array of
                length N, where each element is an integer giving the predicted
                class.
        """
        # TODO: implement me
        result = np.zeros(X_test.shape[0])
        for i in range(X_test.shape[0]):
            raw_result = np.dot(self.w, np.append(X_test[i], 1))
            prob = Softmax.softmax(raw_result)
            max_val = max(prob)
            result[i] = list(prob).index(max_val)
        return result
