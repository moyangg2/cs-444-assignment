"""Perceptron model."""

import numpy as np


class Perceptron:
    def __init__(self, n_class: int, lr: float, epochs: int):
        """Initialize a new classifier.

        Parameters:
            n_class: the number of classes
            lr: the learning rate
            epochs: the number of epochs to train for
        """
        self.w = None  # TODO: change this
        self.lr = lr
        self.epochs = epochs
        self.n_class = n_class

    def train(self, X_train: np.ndarray, y_train: np.ndarray):
        """Train the classifier.

        Use the perceptron update rule as introduced in the Lecture.

        Parameters:
            X_train: a number array of shape (N, D) containing training data;
                N examples with D dimensions
            y_train: a numpy array of shape (N,) containing training labels
        """
        # TODO: implement me
        input_size = X_train.shape[1] + 1
        self.w = np.random.random((self.n_class, input_size))                           # Each row of this 2d-array is a weight for a class.
        for i in range(self.epochs):
            for j in range(len(y_train)):
                x = np.append(X_train[j], 1)
                label = y_train[j]
                prediction_value = np.dot(self.w, x)
                for k in range(self.n_class):
                    if (prediction_value[k] > prediction_value[label]):        # Check whether predict wrongly.
                        self.w[label] += self.lr * x
                        self.w[k] -= self.lr * x

    def predict(self, X_test: np.ndarray) -> np.ndarray:
        """Use the trained weights to predict labels for test data points.

        Parameters:
            X_test: a numpy array of shape (N, D) containing testing data;
                N examples with D dimensions

        Returns:
            predicted labels for the data in X_test; a 1-dimensional array of
                length N, where each element is an integer giving the predicted
                class.
        """
        # TODO: implement me
        prediction = np.zeros(X_test.shape[0])
        for i in range(X_test.shape[0]):
            result = list(np.dot(self.w, np.append(X_test[i], 1)))
            max_val = max(result)
            label = result.index(max_val)
            prediction[i] = label
        return prediction
