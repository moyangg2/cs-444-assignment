"""Neural network model."""

from typing import Sequence

import numpy as np


class NeuralNetwork:
    """A multi-layer fully-connected neural network. The net has an input
    dimension of N, a hidden layer dimension of H, and performs classification
    over C classes. We train the network with a cross-entropy loss function and
    L2 regularization on the weight matrices.
    The network uses a nonlinearity after each fully connected layer except for
    the last. The outputs of the last fully-connected layer are passed through
    a softmax, and become the scores for each class."""

    def __init__(
        self,
        input_size: int,
        hidden_sizes: Sequence[int],
        output_size: int,
        num_layers: int
    ):
        """Initialize the model. Weights are initialized to small random values
        and biases are initialized to zero. Weights and biases are stored in
        the variable self.params, which is a dictionary with the following
        keys:
        W1: 1st layer weights; has shape (D, H_1)
        b1: 1st layer biases; has shape (H_1,)
        ...
        Wk: kth layer weights; has shape (H_{k-1}, C)
        bk: kth layer biases; has shape (C,)
        Parameters:
            input_size: The dimension D of the input data
            hidden_size: List [H1,..., Hk] with the number of neurons Hi in the
                hidden layer i
            output_size: The number of classes C
            num_layers: Number of fully connected layers in the neural network
        """
        self.input_size = input_size
        self.hidden_sizes = hidden_sizes
        self.output_size = output_size
        self.num_layers = num_layers

        assert len(hidden_sizes) == (num_layers - 1)
        self.sizes = [input_size] + hidden_sizes + [output_size]

        self.params = {}
        self.v = {}
        self.m = {}
        for i in range(1, num_layers + 1):
            self.params["W" + str(i)] = np.random.randn(
                self.sizes[i - 1], self.sizes[i]
            ) / np.sqrt(self.sizes[i - 1])
            self.params["b" + str(i)] = np.zeros(self.sizes[i])
            self.v["v" + str(i)] = np.zeros(self.sizes[i])
            self.m["m" + str(i)] = np.zeros((self.sizes[i-1], self.sizes[i]))

    def linear(self, W: np.ndarray, X: np.ndarray, b: np.ndarray) -> np.ndarray:
        """Fully connected (linear) layer.
        Parameters:
            W: the weight matrix
            X: the input data
            b: the bias
        Returns:
            the output
        """
        # TODO: implement me
        b_new = np.zeros((X.shape[0], W.shape[1]))
        for i in range(X.shape[0]):
            b_new[i] = b
        result = np.dot(X, W) + b_new
        return result

    def relu(self, X: np.ndarray) -> np.ndarray:
        """Rectified Linear Unit (ReLU).
        Parameters:
            X: the input data
        Returns:
            the output
        """
        # TODO: implement me
        result = np.maximum(X, 0)
        return result

    def relu_grad(self, X: np.ndarray) -> np.ndarray:
        """Gradient of Rectified Linear Unit (ReLU).
        Parameters:
            X: the input data
        Returns:
            the output data
        """
        # TODO: implement me
        result = np.zeros(X.size)
        for i in range(X.size):
            if (X[i] > 0):
                result[i] = 1
        return result

    def softmax(self, X: np.ndarray) -> np.ndarray:
        """The softmax function.
        Parameters:
            X: the input data
        Returns:
            the output
        """
        # TODO: implement me
        result = np.zeros(X.shape)
        for i in range(X.shape[0]):
            vector = X[i]
            Sum = 0
            max_val = max(vector)
            vector -= max_val
            for j in range(len(vector)):
                Sum += np.e ** vector[j]
            for j in range(len(vector)):
                result[i][j] = np.e ** vector[j] / Sum
        return result

    def forward(self, X: np.ndarray) -> np.ndarray:
        """Compute the scores for each class for all of the data samples.
        Hint: this function is also used for prediction.
        Parameters:
            X: Input data of shape (N, D). Each X[i] is a training or
                testing sample
        Returns:
            Matrix of shape (N, C) where scores[i, c] is the score for class
                c on input X[i] outputted from the last layer of your network
        """
        self.outputs = {}
        # TODO: implement me. You'll want to store the output of each layer in
        # self.outputs as it will be used during back-propagation. You can use
        # the same keys as self.params. You can use functions like
        # self.linear, self.relu, and self.softmax in here.
        self.outputs["M" + str(0)] = X
        for i in range(1, self.num_layers):
            X = self.linear(self.params["W" + str(i)], X, self.params["b" + str(i)])
            self.outputs["M" + str(2*i-1)] = X
            X = self.relu(X)
            self.outputs["M" + str(2*i)] = X
        X = self.linear(self.params["W" + str(self.num_layers)], X, self.params["b" + str(self.num_layers)])
        self.outputs["M" + str(2 * self.num_layers - 1)] = X
        result = self.softmax(X)
        self.outputs["M" + str(2 * self.num_layers)] = result
        return result

    def backward(self, y: np.ndarray, reg: float = 0.0) -> float:
        """Perform back-propagation and compute the gradients and losses.
        Note: both gradients and loss should include regularization.
        Parameters:
            y: Vector of training labels. y[i] is the label for X[i], and each
                y[i] is an integer in the range 0 <= y[i] < C
            reg: Regularization strength
        Returns:
            Total loss for this batch of training samples
        """
        self.gradients = {}
        # TODO: implement me. You'll want to store the gradient of each
        # parameter in self.gradients as it will be used when updating each
        # parameter and during numerical gradient checks. You can use the same
        # keys as self.params. You can add functions like self.linear_grad,
        # self.relu_grad, and self.softmax_grad if it helps organize your code.
        loss = 0
        for i in range(1, self.num_layers + 1):
            self.gradients["WB" + str(i)] = np.zeros((self.sizes[i - 1], self.sizes[i]))
            self.gradients["bB" + str(i)] = np.zeros(self.sizes[i])
        for i in range(y.size):
            label = y[i]
            loss += -np.log(self.outputs["M" + str(2 * self.num_layers)][i][label])
            for j in range(self.output_size):
                if (j == label):
                    self.gradients["WB" + str(self.num_layers)][:, j] += (self.outputs["M" + str(2 * self.num_layers)][i][j] - 1) * self.outputs["M" + str(2 * self.num_layers - 2)][i]
                    self.gradients["bB" + str(self.num_layers)][j] = (self.outputs["M" + str(2 * self.num_layers)][i][j] - 1)
                else:
                    self.gradients["WB" + str(self.num_layers)][:, j] += self.outputs["M" + str(2 * self.num_layers)][i][j] * self.outputs["M" + str(2 * self.num_layers - 2)][i]
                    self.gradients["bB" + str(self.num_layers)][j] = self.outputs["M" + str(2 * self.num_layers)][i][j]
                self.gradients["WB" + str(self.num_layers)] += (reg / 50000) * self.params["W" + str(self.num_layers)]
            inter_gradient = np.zeros(self.sizes[self.num_layers - 1])
            for j in range(self.output_size):
                inter_gradient += self.params["W" + str(self.num_layers)][:, j] * self.outputs["M" + str(self.num_layers * 2)][i][j]
            inter_gradient -= self.params["W" + str(self.num_layers)][:, label]
            for j in range(1, self.num_layers):
                offset = 2 * self.num_layers - 2 * j
                inter_gradient *= self.relu_grad(self.outputs["M" + str(offset - 1)][i])
                for k in range(self.gradients["WB" + str(self.num_layers - j)].shape[1]):
                    self.gradients["WB" + str(self.num_layers - j)][:, k] += self.outputs["M" + str(offset - 2)][i] * inter_gradient[k]
                self.gradients["bB" + str(self.num_layers - j)] += inter_gradient
                self.gradients["WB" + str(self.num_layers - j)] += (reg / 50000) * self.params["W" + str(self.num_layers - j)]
                inter_gradient_ = np.dot(inter_gradient, self.params["W" + str(self.num_layers - j)].T)
                inter_gradient = inter_gradient_
        for i in range(1, self.num_layers + 1):
            self.gradients["WB" + str(i)] *= 1 / y.size
            self.gradients["bB" + str(i)] *= 1 / y.size
        return loss

    def update(
        self,
        lr: float = 0.001,
        b1: float = 0.9,
        b2: float = 0.999,
        eps: float = 1e-8,
        opt: str = "SGD",
    ):
        """Update the parameters of the model using the previously calculated
        gradients.
        Parameters:
            lr: Learning rate
            b1: beta 1 parameter (for Adam)
            b2: beta 2 parameter (for Adam)
            eps: epsilon to prevent division by zero (for Adam)
            opt: optimizer, either 'SGD' or 'Adam'
        """
        # TODO: implement me. You'll want to add an if-statement that can
        # handle updates for both SGD and Adam depending on the value of opt.
        if (opt == "SGD"):
            for i in range(1, self.num_layers + 1):
                self.params["W" + str(i)] -= lr * self.gradients["WB" + str(i)]
                self.params["b" + str(i)] -= lr * self.gradients["bB" + str(i)]
        elif (opt == "Adam"):
            for i in range(1, self.num_layers + 1):
                self.m["m" + str(i)] = b1 * self.m["m" + str(i)] + (1 - b1) * self.gradients["WB" + str(i)]
                for j in range(self.v["v" + str(i)].size):
                    self.v["v" + str(i)][j] = b2 * self.v["v" + str(i)][j] + (1 - b2) * np.dot(self.gradients["WB" + str(i)][:, j], self.gradients["WB" + str(i)][:, j])
                    self.params["W" + str(i)][:, j] -= lr / (self.v["v" + str(i)][j] ** 0.5 + eps) * self.m["m" + str(i)][:, j]
        return



#for i in range(self.output_size):
#            for j in range(self.input_size):
#                label = y[j]
#                if (label == i):
#                    self.gradients["WB" + str(self.num_layers)][:][i] += (self.outputs["M" + str(2 * self.num_layers)][j][i] - 1) * self.outputs["M" + str(2 * self.num_layers - 2)][j]
#                    self.gradients["bB" + str(self.num_layers)] += self.outputs["M" + str(2 * self.num_layers)][j][i] - 1
#                else:
#                    self.gradients["WB" + str(self.num_layers)][:][i] += self.outputs["M" + str(2 * self.num_layers)][j][i] * self.outputs["M" + str(2 * self.num_layers - 2)][j]
#                    self.gradients["bB" + str(self.num_layers)] += self.outputs["M" + str(2 * self.num_layers)][j][i]
#        inter_gradient = np.zeros((self.input_size, sizes[self.num_layers - 1]))
#        for i in range(self.input_size):
#            label = y[i]
#            for j in range(self.output_size):
#                inter_gradient += self.outputs["M" + str(2 * self.num_layers)][i][j] * self.params["W" + str(self.num_layers)][:][j]
#            inter_gradient -= self.params["W" + str(self.num_layers)][:][label]
#        for i in range(1, self.num_layers):
#            offset = 2 * self.num_layers - 2 * i
#            inter_gradient *= relu_grad(self, self.outputs["M" + str(offset - 1)])
#            self.gradients["WB" + str(self.num_layers - j)] = np.dot(self.outputs["M" + str(offset - 2)].T, inter_gradient)
#            inter_gradient
#        return
