"""Perceptron model."""

import numpy as np
import pdb


class Perceptron:
    def __init__(self, n_class: int, lr: float, epochs: int):
        """Initialize a new classifier.

        Parameters:
            n_class: the number of classes
            lr: the learning rate
            epochs: the number of epochs to train for
        """
        self.w = None # TODO: change this
        self.lr = lr
        self.epochs = epochs
        self.n_class = n_class
        self.lr_decay=0.01
        self.lam= 1e-4
        return
    def prediction(self, X_train:np.array)->np.array:
        result= np.dot(X_train,self.w)
        return result



    def train(self, X_train: np.ndarray, y_train: np.ndarray)->None:
        """Train the classifier.

        Use the perceptron update rule as introduced in the Lecture.

        Parameters:
            X_train: a number array of shape (N, D) containing training data;
                N examples with D dimensions
            y_train: a numpy array of shape (N,) containing training labels
        """
        self.w= np.ones((X_train.shape[-1],self.n_class)) # D * C
        for epoch in range (self.epochs):
            prediction=self.prediction(X_train)
            for i in range (X_train.shape[0]):
                for class_num in range (self.n_class):
                    if (prediction[i,class_num]>=prediction[i,y_train[i]]):
                        self.w[:,class_num]-=self.lr*X_train[i]
                        self.w[:,y_train[i]]+=self.lr*X_train[i]
            self.lr*=(1-self.lr_decay)

        return

    def predict(self, X_test: np.ndarray) -> np.ndarray:
        """Use the trained weights to predict labels for test data points.

        Parameters:
            X_test: a numpy array of shape (N, D) containing testing data;
                N examples with D dimensions

        Returns:
            predicted labels for the data in X_test; a 1-dimensional array of
                length N, where each element is an integer giving the predicted
                class.
        """
        return np.argmax(self.prediction(X_test),axis=-1)
