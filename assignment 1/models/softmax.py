"""Softmax model."""

import numpy as np


class Softmax:
    def __init__(self, n_class: int, lr: float, epochs: int, reg_const: float):
        """Initialize a new classifier.

        Parameters:
            n_class: the number of classes
            lr: the learning rate
            epochs: the number of epochs to train for
            reg_const: the regularization constant
        """
        self.w = None  # TODO: change this
        self.lr = lr
        self.epochs = epochs
        self.reg_const = reg_const
        self.n_class = n_class
        self.lr_decay=0.01
        self.lam= 1e-4

    def softmax(self,x:np.array)->np.array:
        x = np.exp(x-np.max(x))/np.sum(np.exp(x-np.max(x)))
        return x
    def prediction(self, x: np.ndarray) -> np.ndarray:
        output= np.dot (x,self.w)
        output=self.softmax(output)

        return output

    def calc_gradient(self, X_train: np.ndarray, y_train: np.ndarray) -> np.ndarray:
        """Calculate gradient of the softmax loss.

        Inputs have dimension D, there are C classes, and we operate on
        mini-batches of N examples.

        Parameters:
            X_train: a numpy array of shape (N, D) containing a mini-batch
                of data
            y_train: a numpy array of shape (N,) containing training labels;
                y[i] = c means that X[i] has label c, where 0 <= c < C

        Returns:
            gradient with respect to weights w; an array of same shape as w
        """
        # TODO: implement me\
        
        for i in range (X_train.shape[0]):
            prediction=self.prediction(X_train[i])
            for class_num in range (self.n_class):
                if class_num==y_train[i]:
                    self.w[:,class_num]-=self.lr*X_train[i]*(prediction[class_num]-1)
                else:
                    self.w[:,class_num]-=self.lr*X_train[i]*(prediction[class_num])
        return

    def train(self, X_train: np.ndarray, y_train: np.ndarray):
        """Train the classifier.

        Hint: operate on mini-batches of data for SGD.

        Parameters:
            X_train: a numpy array of shape (N, D) containing training data;
                N examples with D dimensions
            y_train: a numpy array of shape (N,) containing training labels
        """
        self.w= np.ones ((X_train.shape[-1],self.n_class))
        for epoch in range (self.epochs):
            self.calc_gradient(X_train,y_train)
            print(self.lr,epoch)
            self.lr*=(1-self.lr_decay)
        
        return

    def predict(self, X_test: np.ndarray) -> np.ndarray:
        """Use the trained weights to predict labels for test data points.

        Parameters:
            X_test: a numpy array of shape (N, D) containing testing data;
                N examples with D dimensions

        Returns:
            predicted labels for the data in X_test; a 1-dimensional array of
                length N, where each element is an integer giving the predicted
                class.
        """
        # TODO: implement me
        prediction=np.zeros((X_test.shape[0]))
        for i in range (X_test.shape[0]):
            prediction[i]=np.argmax(self.prediction(X_test[i]),axis=-1)
        return prediction
